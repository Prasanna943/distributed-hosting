class Message {
    constructor(messageType, socketInfo, payload, transferMethod){
        this.messageType = messageType;
        this.id = socketInfo.id; 
        this.bufferLength = Buffer.byteLength(payload); 
        this.host = socketInfo.host; 
        this.port = socketInfo.port;
        this.transferMethod = transferMethod;
        this.payload = payload.toString();
    }
}

module.exports = Message;