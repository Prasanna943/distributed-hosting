const crypto = require('crypto')
const Swarm = require('discovery-swarm')
const defaults = require('dat-swarm-defaults')
const getPort = require('get-port')
const readline = require('readline')
const Message = require('./messages')
const blockChain = require('../dat/blockchain/blockchain')
const ecdsa = require('secp256k1')
const http = require('http')
const fs = require('fs')

const blockfiles = fs.readdirSync('block-files');


http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  var resJson = [];
  var tableContent = '';
  for(var i = 0; i < blockfiles.length; i++)
  {
    var curFileData = JSON.parse(fs.readFileSync(`block-files/${blockfiles[i]}`, 'utf8'));
    for(var j = 0; j < curFileData.length; j++)
    {
      for(var k = 0; k < curFileData[j].transactions.length; k++)
      {
        var curTrans = curFileData[j].transactions[k];
        var buff = Buffer.from((curTrans.creator).data);
        tableContent += `
          <tr>
            <td>${curFileData[j].index}</td>
            <td>${Buffer.from((curTrans.data).data).toString('utf8')}</td>
            <td>${curTrans.timeStamp}</td>
            <td>${Buffer.from((curTrans.creator).data).toString('hex')}</td>
          </tr>
        `
      }
    }
    
  }
  var html = `
    <html>
      <head>
        <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
      </head>
      <body>
        <table>
          <tr>
            <td>Block Index</td>
            <td>Data</td>
            <td>Timestamp</td>
            <td>Creator</td>
          </tr>
          ${tableContent}
        </table>
      </body>
    </html>
  `
  res.write(html);
  res.end();
}).listen(8080)

const blockChainObject = new blockChain();

/**
 * Here we will save our TCP peer connections
 * using the peer id as key: { peer_id: TCP_Connection }
 */
const peers = {}
// Counter for connections, used for identify connections
let connSeq = 0

const myPrivateKey = 'bac2c2d464b2a0bea27da49a205a0f5c1d3f829b31af045bf7d524c6dc47301b'
const privateKeyBuffer = Buffer.alloc(32, myPrivateKey, 'hex')
const publicKeyBuffer = ecdsa.publicKeyCreate(privateKeyBuffer)
const myId = publicKeyBuffer;

// Peer Identity, a random hash for identify your peer
console.log('Your identity: ' + myId.toString('hex'))

// reference to redline interface
let rl
/**
 * Function for safely call console.log with readline interface active
 */
function log () {
  if (rl) {
    rl.clearLine()    
    rl.close()
    rl = undefined
  }
  for (let i = 0, len = arguments.length; i < len; i++) {
    console.log(arguments[i])
  }
  askUser()
}

/*
* Function to get text input from user and send it to other peers
* Like a chat :)
*/
const askUser = async () => {
  rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })
  
  rl.question('Send message: ', message => {
    // Broadcast to peers
    
    for (let id in peers) {
      peers[id].conn.write(JSON.stringify( new Message('newPost', peers[id].info, message, 'broadcast') ))
    }
    rl.close()
    rl = undefined
    askUser()
    blockChainObject.createNewTransaction(message, myId, privateKeyBuffer)
      .then( transaction => {

      })
      .catch( err => {

      });
  });
}

/** 
 * Default DNS and DHT servers
 * This servers are used for peer discovery and establishing connection
 */
const config = defaults({
  // peer-id
  id: myId,
})

/**
 * discovery-swarm library establishes a TCP p2p connection and uses
 * discovery-channel library for peer discovery
 */
const sw = Swarm(config);

(async () => {

  // Choose a random unused port for listening TCP peer connections
  const port = await getPort()

  sw.listen(port)
  console.log('Listening to port: ' + port)

  /**
   * The channel we are connecting to.
   * Peers should discover other peers in this channel
   */
  sw.join('project-test-channel')

  sw.on('connection', (conn, info) => {
    // Connection id
    const seq = connSeq

    const peerId = info.id.toString('hex')
    log(`Connected #${seq} to peer: ${peerId}`)

    // Keep alive TCP connection with peer
    if (info.initiator) {
      try {
        conn.setKeepAlive(true, 600)
      } catch (exception) {
        log('exception', exception)
      }
    }

    conn.on('data', data => {

      data = JSON.parse(data.toString())

      blockChainObject.createNewTransaction(data.payload, myId, privateKeyBuffer)
                      .then( transaction => {

                      })
                      .catch( err => {

                      });

      /* log(blockChainObject.pendingTransactions) */

    })

    conn.on('close', () => {
      // Here we handle peer disconnection
      log(`Connection ${seq} closed, peer id: ${peerId}`)
      // If the closing connection is the last connection with the peer, removes the peer
      if (peers[peerId].seq === seq) {
        delete peers[peerId]
      }
    })

    // Save the connection
    if (!peers[peerId]) {
      peers[peerId] = {}
    }
    peers[peerId].conn = conn
    peers[peerId].info = info
    peers[peerId].seq = seq
    connSeq++
  })

  // Read user message from command line
  askUser()  

})()