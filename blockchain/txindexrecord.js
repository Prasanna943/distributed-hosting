class TxIndexRecord {
    constructor(blk_file_no, index_in_file, index_in_blk) {
        this.blk_file_no = blk_file_no; 
        this.index_in_file = index_in_file; 
        this.index_in_blk = index_in_blk; 
    }
}

module.exports = TxIndexRecord;