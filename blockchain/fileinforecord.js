class FileInfoRecord {
    constructor(fileInfo) {
        this.blks_count = fileInfo.blks_count; 
        this.l_blk_no = fileInfo.l_blk_no; 
        this.h_blk_no = fileInfo.h_blk_no; 
        this.l_blk_ts = fileInfo.l_blk_ts; 
        this.h_blk_ts = fileInfo.h_blk_ts;
    }
}

module.exports = FileInfoRecord;