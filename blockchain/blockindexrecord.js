class BlockIndexRecord {
    constructor(blk_header, blk_height, tx_count, blk_file, index_in_file) {
        this.blk_header = blk_header; 
        this.blk_height = blk_height; 
        this.tx_count = tx_count; 
        this.blk_file = blk_file; 
        this.index_in_file = index_in_file;
    }
}

module.exports = BlockIndexRecord;