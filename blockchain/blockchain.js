const ecdsa = require('secp256k1')
const crypto = require('crypto')
const fs = require('fs') 
var level = require('level')

var fileInfoRecord = require('../blockchain/fileinforecord')
var blockIndexRecord = require('../blockchain/blockindexrecord')
var txIndexRecord = require('../blockchain/txindexrecord')

const blockFilesDirectory = 'block-files/'
var db = level('my-db')

class Blockchain {
        constructor() {
            this.genesis = {
                index : 0,
                timeStamp : 123456789,
                transactions : {
                    
                },
                nonce : 5,
                hash : '000000000000000000000000000000000000000000000000000000000000',
                previousBlockHash : '000000000000000000000000000000000000000000000000000000000000', 
            };
            this.pendingTransactions = [];

            /* ..Have to code the damn genisis block here.. */
        }

        // Invoke to create a new block and add that block to chain
        createNewBlock() {
            var obj = this;
            db.get('l', function(err, val) {
                console.log(err, val);
                var newBlock = {
                    index : 1,
                    timeStamp : 0,
                    transactions : obj.pendingTransactions,
                    nonce : 0,
                    hash : '',
                    previousBlockHash : obj.genesis.hash, 
                };

                if(err || !val)
                {
                    /* obj.writeBlockToFile(obj.genesis); 
                    return; */

                    var pow = obj.proofOfWork(newBlock.previousBlockHash, newBlock.transactions);

                    newBlock.timeStamp = Date.now();
                    newBlock.nonce = pow.nonce; 
                    newBlock.hash = pow.hash;

                    obj.pendingTransactions = [];

                    obj.writeBlockToFile(newBlock);
                }
                else
                {
                    fs.readFile(blockFilesDirectory + 'blk' + val + '.json', 'utf8', function(err, lastFile) {
                        if(err) throw err;
                        lastFile = JSON.parse(lastFile);
                        var lastBlock = lastFile[lastFile.length - 1];

                        newBlock.index = lastBlock.index + 1;
                        newBlock.previousBlockHash = lastBlock.hash;

                        var pow = obj.proofOfWork(newBlock.previousBlockHash, newBlock.transactions);

                        newBlock.timeStamp = Date.now();
                        newBlock.nonce = pow.nonce; 
                        newBlock.hash = pow.hash;

                        obj.pendingTransactions = [];

                        obj.writeBlockToFile(newBlock);
                    });
                }                
            });
        }

        writeBlockToFile(blockData) {
            fs.access(blockFilesDirectory + 'blk00.json', fs.constants.F_OK, (err) => {
                if(err)
                {
                    var dbOps = [
                        {
                            type: 'put', 
                            key: 'l', 
                            value: '00'
                        }
                    ];

                    dbOps.push(
                        {
                            type: 'put', 
                            key: 'f00', 
                            value: JSON.stringify(
                                new fileInfoRecord({
                                    blks_count: 1, 
                                    l_blk_no: 0, 
                                    h_blk_no: 0, 
                                    l_blk_ts: blockData.timeStamp, 
                                    h_blk_ts: blockData.timeStamp
                                })
                            )
                        }
                    );

                    dbOps.push(
                        {
                            type: 'put', 
                            key: 'b' + blockData.hash, 
                            value: JSON.stringify(
                                new blockIndexRecord({
                                    blk_header: {
                                        prev_hash: blockData.previousBlockHash, 
                                        timeStamp: blockData.timeStamp, 
                                        nonce: blockData.nonce
                                    }, 
                                    blk_height: 0, 
                                    tx_count: 10, 
                                    blk_file: '00', 
                                    index_in_file: 0
                                })
                            ) 
                        }
                    ); 

                    for(var i = 0; i < blockData.transactions.length; i++)
                    {
                        dbOps.push( 
                            {
                                type: 'put', 
                                key: 't' + blockData.transactions[i].txHash, 
                                value: JSON.stringify(
                                    new txIndexRecord({
                                        blk_file_no: '00', 
                                        index_in_file: 0, 
                                        index_in_blk: i
                                    })
                                )
                            }
                        );
                    }

                    
                    db.batch(dbOps, function (err) {
                        if(err) console.log(err);   
                                             
                        fs.writeFile(blockFilesDirectory + 'blk00.json', JSON.stringify([blockData]), 'utf8', function(err) {
                            if(err) console.log(err);
                        });

                        /* db.get('f00', function(err, val) {
                            console.log(val);
                        }) */
                   });
                }
                else
                {
                    db.get('l', function(err, lastFileName) {
                        if(!err)
                        {
                            db.get('f' + lastFileName, function(err, lastFileInfo) {
                                if(!err)
                                {
                                    lastFileInfo = JSON.parse(lastFileInfo); 
                                    
                                    fs.readFile(blockFilesDirectory + 'blk' + lastFileName + '.json', 'utf8', function(err, lastFileData) {

                                        lastFileData = JSON.parse(lastFileData);
                                        
                                        if(lastFileInfo.blks_count < 2)
                                        {
                                            var dbOps = [
                                                {
                                                    type: 'put', 
                                                    key: 'l', 
                                                    value: lastFileName
                                                }
                                            ];
                        
                                            dbOps.push(
                                                {
                                                    type: 'put', 
                                                    key: 'f' + lastFileName, 
                                                    value: JSON.stringify(
                                                        new fileInfoRecord({
                                                            blks_count: lastFileInfo.blks_count + 1, 
                                                            l_blk_no: lastFileData[0].index, 
                                                            h_blk_no: blockData.index, 
                                                            l_blk_ts: lastFileData[0].timeStamp, 
                                                            h_blk_ts: blockData.timeStamp
                                                        })
                                                    )
                                                }
                                            );
                        
                                            dbOps.push(
                                                {
                                                    type: 'put', 
                                                    key: 'b' + blockData.hash, 
                                                    value: JSON.stringify(
                                                        new blockIndexRecord({
                                                            blk_header: {
                                                                prev_hash: blockData.previousBlockHash, 
                                                                timeStamp: blockData.timeStamp, 
                                                                nonce: blockData.nonce
                                                            }, 
                                                            blk_height: blockData.index, 
                                                            tx_count: 10, 
                                                            blk_file: lastFileName, 
                                                            index_in_file: lastFileData.length
                                                        })
                                                    ) 
                                                }
                                            ); 
                        
                                            for(var i = 0; i < blockData.transactions.length; i++)
                                            {
                                                dbOps.push( 
                                                    {
                                                        type: 'put', 
                                                        key: 't' + blockData.transactions[i].txHash, 
                                                        value: JSON.stringify(
                                                            new txIndexRecord({
                                                                blk_file_no: lastFileName, 
                                                                index_in_file: lastFileData.length, 
                                                                index_in_blk: i
                                                            })
                                                        )
                                                    }
                                                );
                                            }

                                            db.batch(dbOps, function (err) {
                                                if(err) console.log(err);   
                                                        
                                                lastFileData.push(blockData)
                                                fs.writeFile(blockFilesDirectory + 'blk' + lastFileName +'.json', JSON.stringify(lastFileData), 'utf8', function(err) {
                                                    if(err) console.log(err);

                                                });
                                            });
                                        }
                                        else
                                        {
                                            var lastFileNameInt = parseInt(lastFileName) + 1;
                                            var newFileName = lastFileNameInt > 9 ? lastFileNameInt.toString() : '0' + lastFileNameInt.toString();

                                            console.log(newFileName);

                                            var dbOps = [
                                                {
                                                    type: 'put', 
                                                    key: 'l', 
                                                    value: newFileName
                                                }
                                            ];
                        
                                            dbOps.push(
                                                {
                                                    type: 'put', 
                                                    key: 'f' + newFileName, 
                                                    value: JSON.stringify(
                                                        new fileInfoRecord({
                                                            blks_count: 1, 
                                                            l_blk_no: blockData.index, 
                                                            h_blk_no: blockData.index, 
                                                            l_blk_ts: blockData.timeStamp, 
                                                            h_blk_ts: blockData.timeStamp
                                                        })
                                                    )
                                                }
                                            );
                        
                                            dbOps.push(
                                                {
                                                    type: 'put', 
                                                    key: 'b' + blockData.hash, 
                                                    value: JSON.stringify(
                                                        new blockIndexRecord({
                                                            blk_header: {
                                                                prev_hash: blockData.previousBlockHash, 
                                                                timeStamp: blockData.timeStamp, 
                                                                nonce: blockData.nonce
                                                            }, 
                                                            blk_height: blockData.index, 
                                                            tx_count: 10, 
                                                            blk_file: newFileName, 
                                                            index_in_file: 0
                                                        })
                                                    ) 
                                                }
                                            ); 
                        
                                            for(var i = 0; i < blockData.transactions.length; i++)
                                            {
                                                dbOps.push( 
                                                    {
                                                        type: 'put', 
                                                        key: 't' + blockData.transactions[i].txHash, 
                                                        value: JSON.stringify(
                                                            new txIndexRecord({
                                                                blk_file_no: newFileName, 
                                                                index_in_file: 0, 
                                                                index_in_blk: i
                                                            })
                                                        )
                                                    }
                                                );
                                            }

                                            db.batch(dbOps, function (err) {
                                                if(err) console.log(err);   
                                                        
                                                fs.writeFile(blockFilesDirectory + 'blk' + newFileName +'.json', JSON.stringify([blockData]), 'utf8', function(err) {
                                                    if(err) console.log(err);

                                                });
                                            });
                                        }
                                    });
                                }
                            })
                        }
                    });
                }
            });
        }

        // returns the last block of the blockchain from chain[]
        getLastBlock() {
            return this.chain[this.chain.length - 1];
        }

        // Invoke this with parameters to create new transactions
        createNewTransaction(data, creator, privateKey) {
            var obj = this;
            return new Promise(function (resolve, reject) {
                data = Buffer.from(data)
                var transaction = {
                    data : data,
                    creator : creator, 
                    timeStamp : Date.now(), 
                    signature : Buffer.alloc(64, obj.getSignature(data, privateKey), 'hex'),
                };
                
                transaction.txHash = obj.sha256( JSON.stringify(transaction) );


                if(obj.verifySignature(data, transaction.signature, transaction.creator))
                {
                    obj.pendingTransactions.push(transaction);
                }

                if(obj.pendingTransactions.length == 2)
                {
                    obj.createNewBlock(); 
                }
                resolve('promise');
            });
            
        }

        getSignature(data, privateKey) {
            const dataHash = this.sha256(data)
            const signature = ecdsa.sign(dataHash, privateKey).signature
            return signature;
        }     

        // invoke this with parameters to obtain block hash(this method has to be invoked by proofOfWork() method)
        hashBlock(previousBlockHash, currentBlockData, nonce) {
            const stringToHash = previousBlockHash + JSON.stringify(currentBlockData) + nonce.toString();
            const hash = this.sha256(stringToHash);
            return hash.toString('hex');
        }

        // this method is used to perform proof-of-work(find the correct nonce)
        proofOfWork(previousBlockHash, currentBlockData) {
            console.log('POW running');
            let nonce = 0;
            let hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);
            while(hash.substring(0, 1) !== '0')
            {
                nonce++;
                hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);
            }
            return {
                hash: hash, 
                nonce: nonce
            }; //this nonce value has to be broadcasted to all the nodes on the network
        }

        sha256(plainText) {
            return crypto.createHash('sha256').update(plainText).digest();
        }

        /* getSignature(data, privateKey) {
            const dataHash = crypto.createHash('sha256').update(data).digest();
            return ecdsa.sign(dataHash, privateKey).signature;
        }  */    
        
        /* verifySignature(data, signature, publicKey) {
            return ecdsa.verify(data, signature, publicKey)
        } */

        verifySignature(data, signature, publicKey) {
            const dataHash = this.sha256(data)
            return ecdsa.verify(dataHash, signature, publicKey)
        }
}

module.exports = Blockchain;