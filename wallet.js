const crypto = require('crypto')
const readline = require('readline')
const base58 = require('bs58')
const ecdsa = require('secp256k1')

let rl;

const askUser = async () => {
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    rl.question('Enter seed for private key: ', userInput => {
        console.log('\n')
        const privateKey = getPrivateKey(userInput)
        console.log('Compressed Private Key: ' + privateKey)

        rl.close()
        rl = undefined
    });
}

function getPrivateKey(userInput)
{
    // finding the SHA256 digest for the user input
    var random256BitNumber = crypto.createHash('sha256').update(userInput).digest('hex');
    console.log('Private Key: ' + random256BitNumber)

    const publicKey = getPublicKey(random256BitNumber)
    console.log('Public Key: ' + publicKey)
    console.log('\n')

    // the version number to be prepended with the private key
    const versionNumber = '80';
    random256BitNumber = versionNumber + random256BitNumber;
    
    // double hash to get the checksum
    const sha1 = crypto.createHash('sha256').update(random256BitNumber, 'hex').digest('hex');
    const sha2 = crypto.createHash('sha256').update(sha1, 'hex').digest('hex');

    const checksum = sha2.substring(0, 8);

    random256BitNumber += checksum;

    // converting the resultant hash into buffer and encoding in base58
    const buffer = Buffer.from(random256BitNumber, 'hex')
    const privateKey = base58.encode(buffer) // the PRIVATE KEY

    return privateKey;
}

function getPublicKey(privateKey)
{
    const privateKeyBuffer = Buffer.alloc(32, privateKey, 'hex')
    const publicKeyBuffer = ecdsa.publicKeyCreate(privateKeyBuffer)

    const signature = ecdsa.sign(Buffer.alloc(32, 'lol'), privateKeyBuffer)
/*     console.log(signature.signature)
 */    const verify = ecdsa.verify(Buffer.alloc(32, 'lol'), signature.signature, publicKeyBuffer)

    /* console.log(verify) */

    return publicKeyBuffer.toString('hex');
}

askUser();